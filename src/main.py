'''
This file is part of AmongUsChatBot.

AmongUsChatBot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

AmongUsChatBot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AmongUsChatBot.  If not, see <https://www.gnu.org/licenses/>.
'''
from pyautogui import click, typewrite
from time import sleep
from dialogconf import dialog

# importing pyautogui to click the chat box and send message, typewrite to typing message
# time to pausing script

# next line need to click the chat icon...
click(x=linex, y=liney)
while True:
    # You need to get the coordinates of some among us interface, check README for more info.
    # next line to click message input box...
    click(x=linex, y=liney)
    # next line to write message
    typewrite(dialog, interval=0.0)
    # next line for cooldown waiting
    sleep(3)
    # next line to send message
    click(x=linex, y=liney)
