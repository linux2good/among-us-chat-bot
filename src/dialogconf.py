'''
This file is part of AmongUsChatBot.

AmongUsChatBot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

AmongUsChatBot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AmongUsChatBot.  If not, see <https://www.gnu.org/licenses/>.
'''
from colorconf import colors
from random import choice

color = choice(colors)
# added second generator for choosing second persons
color2 = choice(colors)

# Please, add your own dialog if you have idea.
dialogs = [
'I saw ' + color + ' vented',
'I saw ' + color + ' killed',
color + ' killed in front of me',
color + ' sus',
'vote ' + color,
# in next line added backslash because python syntax allow apostrophe in string only with backlashes...
'It\'s def ' + color + ' vote him first',
color + ' ignored the body',
color + ' dancing on the body',
'vote ' + color + ' first, then ' + color2,
'vote ' + color + ' first',
'It\'s self report.',
color + ' faked scan',
color + ' faked task',
color + ' faked fix sabotage',
color + ' shifted into ' + color2,
color + ' killed and shifted into ' + color2,
color + ' venting without cooldown',
color + ' vented and chasing me',
color + ' was with him and he died',
color + ' and ' + color2 + ' stood on the body then chasing me',
color + ' wanted to kill me but shield saved me',
'i saw ' + color + ' unshifted',
# in next line i add sherlock and he will told on random people what he is imposter.
'Sherlock trying to find out imposter... Imposter found! Imposter is... ' + color,
'i was locked and ' + color + ' vented and wanted to kill me',
'I saw ' + color + ' killed on cams',
'I\'m Imposters with ' + color,
# in next line bot will accusing someone, what he hacker and for players kicked him from lobby
color + ' Hacker kick him. he hacker because he venting but not on vent'
]

# choosing dialog from dialogs list...
dialog = choice(dialogs)

